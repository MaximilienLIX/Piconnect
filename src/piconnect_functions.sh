#!/usr/bin/env bash

RESET="$(tput sgr0)"
YELLOW="$(tput bold ; tput setaf 3)"
RED="$(tput bold ; tput setaf 1)"
WHITEB="$(tput bold ; tput setaf 7)"

file_config="$HOME/.config/piconnect_config.$1.rc"
file_configP="$HOME/.config/piconnect_config.$2.rc"

verify_config_folder()
{
    if [ ! -d "$HOME/.config" ]
    then
    mkdir .config
    fi
}

manage_profil()
{
    case $1 in
            "ls")
            ls ~/.config | grep piconnect_config.
            ;;
            "cat")
            cat $file_configP
            ;;
            "edit")
            $EDITOR $file_configP
            ;;
            "rm")
            rm $file_configP
            ;;
            "add")

            if [ ! -z $2 ]
            then

     if [ ! -e "$file_configP" ]
     then
     while [ 1 ]
     do
     echo -e "${WHITEB}Création du profil ${YELLOW}$2 !${RESET}\n"
     read -p "Adresse IP de votre raspberry : " ippi
     read -p "Nom utilisateur de votre raspberry : " nompi
     echo -e "\n${YELLOW}Êtes-vous sûr de la configuration ?${RESET}\n\n\tAdresse ip : $ippi\n\tNom utilisateur : $nompi\n\tNom du profil : $2\n"
     read -p "oui | non | q (quitter) -> " rep

     case $rep in
               "oui")
cat > $file_configP << EOF
export ip_address_pi="$ippi"
export host_address_pi="$nompi"
export pi_name="$2"
EOF
                exit 0
                ;;
                "non")
                ;;
                "q")
                exit 0
                ;;
                *)
                echo "${RED}ERREUR${RESET}"
                ;;
      esac
      done
      fi
      fi
            ;;
    esac
    return 1
}

verify_and_execute()
{
    if [ -z $1 ]
    then
    echo -e "${RED}Entre un argument !${RESET}\tPour plus d'information faites ${WHITEB}man piconnect${RESET}"
    return 1
    fi
    if [ -e "$file_config" ]
    then

    source "$file_config"

    case $1 in
           "$pi_name")
           case $2 in
                  "-xorg")
                  vncviewer $ip_address_pi:$3
                  ;;
                  *)
                  ssh $host_address_pi@$ip_address_pi
                  ;;
          esac
          ;;
          *)
          echo -e "${RED}Erreur${RESET}\tFaites ${WHITEB}man piconnect${RESET} pour plus d'information."
          ;;
     esac
     fi
}




