Piconnect
=========

Petit script bash qui permet de vous connecter facilement en SSH avec votre raspberry. 

> Son fonctionnement repose sur l'utilisation de profils.


Pour vous connecter à votre framboise 3.14 il suffit d'entrer le nom du profil correspondant. (qui est en fait un alias)

```bash
piconnect [NOM_DU_PROFIL]
```

Si le nom du profil correspond à `ma-framboise`, alors vous devez taper:

```bash
piconnect ma-framboise
```

# Profil

L'intérêt de Piconnect réside dans le fait qu'il utilise un "profil" pour se connecter au raspberry. Il est possible d'ajouter ou de supprimer un profil via l'argument `add` ou `rm` suivi du nom.

```bash
piconnect add [NOM_DU_PROFIL]
```

Ajoute un profil.

```bash
piconnect rm [NOM_DU_PROFIL]
```

Supprime un profil.

```bash
piconnect ls
```

Donne une liste des profils qui existent sur votre ordinateur.

```bash
piconnect cat [NOM_DU_PROFIL]
```

Affiche le contenu du profil.

```bash
piconnect edit [NOM_DU_PROFIL]
```

Ouvre le profil avec nano.

# Session graphique 

Il est possible d'ouvrir une sesion graphique avec piconnect. Pour cela la commande utilise le système de visualisation VNC. **Le paquet TightVNC doit être installé sur le serveur et le client !** 

Sur le serveur: (le Raspberry)

```bash 
vncserver :1
```

Ouvre une session VNC dont le numéro est 1. De ce fait pour se connecter à la session, le client doit faire:

```bash
piconnect ma-framboise -xorg 1
```

(1 correspondant au numéro de la session qui nous intéresse ! En effet le serveur est en mesure de pouvoir lancer plusieurs sessions VNC. Ainsi plusieurs utilisateurs peuvent démarrer une session graphique sur le raspberry.)

# Installation par AUR

Veuillez utiliser le [PKGBUILD](https://aur.archlinux.org/packages/piconnect-git/). Vous pouvez installer facilement Piconnect via la commande suivante:

```bash
yaourt piconnect-git
```



